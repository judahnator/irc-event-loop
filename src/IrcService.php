<?php

namespace judahnator\IrcEventLoop;

use judahnator\EventLoop\EventLoop;
use Phergie\Irc\Parser;

abstract class IrcService extends EventLoop
{

    private $hasSentIdent = false;
    private $message;
    private $nickname;
    private $socketConnection;

    public function __construct(string $endpointUrl, string $nickname, int $port = 80)
    {
        $this->socketConnection = fsockopen($endpointUrl, $port);
        $this->nickname = $nickname;
    }

    public function __destruct()
    {
        if ($this->socketConnection) {
            fclose($this->socketConnection);
        }
    }

    final public function loopSetup(): void
    {
        $this->duringEvent(function() {
            if ($this->hasSentIdent) {
                return;
            }
            $this->sendMessage("NICK {$this->nickname}");
            $this->sendMessage("USER {$this->nickname} * * :{$this->nickname}");
            $this->hasSentIdent = true;
        });
        $this->duringEvent(function($message) {
            if ($message['command'] === "PING") {
                $this->sendMessage("PONG : {$message['params']['server1']}");
                return false;
            }
            return true;
        });
        $this->setup();
    }

    final public function loopTeardown(): void
    {
        $this->teardown();
    }

    /**
     * If this returns true the loop will run once.
     * If this returns false the loop will exit.
     *
     * This acts as the loop rate-limiter as well.
     *
     * @return bool
     */
    final public function getRunCondition(): bool
    {
        return !feof($this->socketConnection) && $this->message = fgets($this->socketConnection);
    }

    /**
     * The result of this will be passed to the "during event" callbacks.
     *
     * @return mixed
     */
    public function getEventCallbackArgument()
    {
        return (new Parser())->parse($this->message);
    }

    /**
     * @param string $message
     * @throws \WebSocket\BadOpcodeException
     */
    public function sendMessage(string $message): void
    {
        fwrite($this->socketConnection, $message.PHP_EOL);
    }

    abstract public function setup(): void;

    abstract public function teardown(): void;
}